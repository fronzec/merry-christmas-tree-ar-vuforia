﻿using UnityEngine;

public class Rotate : MonoBehaviour
{
    private Transform _transform;

    // Use this for initialization
    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        _transform.Rotate(Vector3.up, 5 * Time.deltaTime, Space.Self);
        
    }
}